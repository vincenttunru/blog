import { glob } from "astro/loaders";
import { defineCollection, z } from "astro:content";
import { rssSchema } from "@astrojs/rss";

const blog = defineCollection({
  // Load Markdown and MDX files in the `src/content/blog/` directory.
  loader: glob({ base: "./src/content/blog", pattern: "**/*.{md,mdx}" }),
  // Type-check frontmatter using a schema
  schema: rssSchema.extend({
    title: z.string(),
    description: z.string().optional(),
    draft: z.boolean().optional(),
    tags: z.union([z.array(z.string()), z.string()]).optional(),
    alias: z.union([z.array(z.string()), z.string()]).optional(),
    image: z.string().optional(),
  	pubDate: z.coerce.date(),
  	updatedDate: z.coerce.date().optional(),
    licence: z.union([z.literal("cc-by"), z.literal("cc-by-sa")]),
  }),
});

export const collections = { blog };
