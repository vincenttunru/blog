import rss from "@astrojs/rss";
import { SITE_TITLE, SITE_DESCRIPTION } from "../consts";
import { getBlogPosts } from "../functions/blog/getBlogPosts";
import { loadRenderers } from "astro:container";
import { getContainerRenderer as getMdxContainerRenderer } from "@astrojs/mdx";
import { experimental_AstroContainer } from "astro/container";
import { render } from "astro:content";
import { getUrl } from "../functions/getUrl";
import { getPostSlug } from "../functions/blog/getPostSlug";

export async function GET(context) {
  const posts = await getBlogPosts();

  const renderers = await loadRenderers([getMdxContainerRenderer()]);
  const container = await experimental_AstroContainer.create({
    renderers: renderers,
  });
  const renderedPosts = await Promise.all(
    posts.map(async (post) => {
      const content = await container.renderToString(
        (await render(post)).Content,
      );
      return {
        ...post,
        content: content,
      };
    }),
  );

  // It looks like I can't make this generate an Atom feed, but Cool URIs don't
  // change - so I'm now publishing a non-Atom feed at atom.xml...
  return rss({
    title: SITE_TITLE,
    description: SITE_DESCRIPTION,
    site: context.site,
    stylesheet: "/rss/styles.xsl",
    items: renderedPosts
      .filter((post) => !post.data.draft)
      .map((post) => ({
        ...post.data,
        link: getUrl(getPostSlug(post.id)),
        content: post.content,
      })),
  });
}
