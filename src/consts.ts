// Place any global data in this file.
// You can import this data from anywhere in your site by using the `import` keyword.

export const SITE_TITLE = "Vincent Tunru.com";
export const SITE_DESCRIPTION =
  "My name is Vincent Tunru, and this is where I write about things that interest me as a front-end engineer.";
