import { SITE_TITLE } from "../../consts";

export type Props = {
  title: string;
};

export const OpenGraphImageContent = (props: Props) => {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        backgroundColor: "#fde68a",
        width: "100%",
        height: "100%",
        padding: "5%",
        borderBottom: "10px solid black",
      }}
    >
      <div style={{ display: "flex", flexGrow: 2 }}>
        <h1 style={{ fontSize: "80px" }}>{props.title}</h1>
      </div>
      <div
        style={{
          display: "flex",
          justifyContent: "flex-end",
          alignItems: "flex-end",
          flexGrow: 1,
          fontSize: "40px",
        }}
      >
        <p>{SITE_TITLE}</p>
      </div>
    </div>
  );
};

export const FallbackOpenGraphImageContent = () => {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        backgroundColor: "#fde68a",
        width: "100%",
        height: "100%",
        padding: "5%",
        borderBottom: "10px solid black",
      }}
    >
      <div style={{ display: "flex", alignItems: "center", flexGrow: 2 }}>
        <h1 style={{ fontSize: "120px" }}>{SITE_TITLE}</h1>
      </div>
      <div
        style={{
          display: "flex",
          justifyContent: "flex-end",
          alignItems: "flex-end",
          flexGrow: 1,
          fontSize: "40px",
        }}
      >
        <p>@VincentTunru@fosstodon.org</p>
      </div>
    </div>
  );
};
