import { mkdir, readFile, writeFile } from "node:fs/promises";
import { resolve } from "node:path";
import satori from "satori";
import sharp from "sharp";
import { FallbackOpenGraphImageContent, OpenGraphImageContent } from "../components/react/OpenGraphImageContent";

export type ImageData = {
  title: string;
};
export async function createOpenGraphImage(data: ImageData): Promise<Buffer<ArrayBufferLike>> {
  const lilitaOne = await readFile(
    resolve(
      import.meta.dirname,
      "../../node_modules/@fontsource/lilita-one/files/lilita-one-latin-400-normal.woff"
    ),
  );

  const svg = await satori(
    <OpenGraphImageContent title={data.title}/>,
    {
      width: 1200,
      height: 630,
      fonts: [
        {
          name: "Lilita One",
          data: lilitaOne,
          weight: 400,
          style: "normal",
        },
      ],
    }
  );

  const png = await sharp(Buffer.from(svg)).png().toBuffer();
  return png;
}

export async function createFallbackOpenGraphImage(): Promise<void> {
  const lilitaOne = await readFile(
    resolve(
      import.meta.dirname,
      "../../node_modules/@fontsource/lilita-one/files/lilita-one-latin-400-normal.woff"
    ),
  );

  const svg = await satori(
    <FallbackOpenGraphImageContent/>,
    {
      width: 1200,
      height: 630,
      fonts: [
        {
          name: "Lilita One",
          data: lilitaOne,
          weight: 400,
          style: "normal",
        },
      ],
    }
  );

  const png = await sharp(Buffer.from(svg)).png().toBuffer();
  await mkdir(
    resolve(import.meta.dirname, "../../public/img/generated/"),
    { recursive: true, }
  );
  await writeFile(
    resolve(import.meta.dirname, "../../public/img/generated/fallbackOgImage.png"),
    png,
  );
}
