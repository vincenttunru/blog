export function getUrl(path: string): string {
  const baseUrl = !import.meta.env.BASE_URL.endsWith("/")
    ? import.meta.env.BASE_URL + "/"
    : import.meta.env.BASE_URL;
  const pathWithoutSlash = path.startsWith("/")
    ? path.substring(1)
    : path;
  const url = new URL(baseUrl + pathWithoutSlash, import.meta.env.SITE);
  return url.href;
}
