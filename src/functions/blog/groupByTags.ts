import type { BlogPost } from "./getBlogPosts";
import { normaliseTags } from "./normaliseBlogPostData";

export type TagGroups = Record<string, BlogPost[]>;

export function groupByTags(posts: BlogPost[]): TagGroups {
  const tagGroups = posts.reduce((acc, post) => {
    const tags = normaliseTags(post);

    tags.forEach((tag) => {
      acc[tag] ??= [];
      acc[tag].push(post);
    });
    return acc;
  }, {} as TagGroups);

  return tagGroups;
}
