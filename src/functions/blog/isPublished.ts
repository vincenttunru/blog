import { CI_COMMIT_BRANCH } from "astro:env/client";
import type { BlogPost } from "./getBlogPosts";

export function isPublished(post: BlogPost): boolean {
  return (
    !post.data.draft ||
    (CI_COMMIT_BRANCH !== "master" && CI_COMMIT_BRANCH !== "main")
  );
}
