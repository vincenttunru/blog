import { getCollection, type CollectionEntry } from "astro:content";
import { isPublished } from "./isPublished";
import { getPostSlug } from "./getPostSlug";
import { createOpenGraphImage } from "../createOpenGraphImage";
import { mkdir, writeFile } from "fs/promises";
import { resolve } from "path";

export type BlogPost = CollectionEntry<"blog">;

const createdImages = [] as string[];

export async function getBlogPosts(): Promise<BlogPost[]> {
  const posts = await getCollection("blog");

  return posts
    .filter((post) => isPublished(post))
    .map((post) => {
      if (!post.data.image) {
        const imagePath = `/img/generated/${getPostSlug(post.id)}/hero.png`;
        if (!createdImages.includes(imagePath)) {
          createOpenGraphImage({ title: post.data.title! }).then(
            async (ogImage) => {
              // TODO: Generate before build?
              const fileLocation = resolve("./dist/", `.${imagePath}`);
              await mkdir(resolve(fileLocation, "../"), { recursive: true });
              await writeFile(fileLocation, ogImage);
            },
          );
          createdImages.push(imagePath);
        }

        return {
          ...post,
          data: {
            ...post.data,
            image: imagePath,
          },
        };
      }

      return post;
    })
    .toSorted(
      (a, b) =>
        (b.data.pubDate?.valueOf() ?? 0) - (a.data.pubDate?.valueOf() ?? 0),
    );
}
