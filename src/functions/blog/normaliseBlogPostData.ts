import type { BlogPost } from "./getBlogPosts";

export function normaliseTags(post: BlogPost): string[] {
  const tags =
    typeof post.data.tags === "string"
      ? [post.data.tags]
      : (post.data.tags ?? []);
  return tags;
}

export function normaliseAliases(post: BlogPost): string[] {
  const aliases: string[] =
    post.data.alias === "string"
      ? [post.data.alias]
      : ((post.data.alias ?? []) as string[]);
  return aliases;
}
