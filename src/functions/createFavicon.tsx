import { mkdir, readFile, writeFile } from "node:fs/promises";
import { resolve } from "node:path";
import satori from "satori";
import sharp from "sharp";
import { FaviconContent } from "../components/react/FaviconContent";

export type ImageData = {
  title: string;
};
export async function createFavicon(): Promise<void> {
  const lilitaOne = await readFile(
    resolve(
      import.meta.dirname,
      "../../node_modules/@fontsource/lilita-one/files/lilita-one-latin-400-normal.woff"
    ),
  );

  const svg = await satori(
    <FaviconContent/>,
    {
      width: 512,
      height: 512,
      fonts: [
        {
          name: "Lilita One",
          data: lilitaOne,
          weight: 400,
          style: "normal",
        },
      ],
    }
  );

  const png = await sharp(Buffer.from(svg)).png().toBuffer();
  await mkdir(
    resolve(import.meta.dirname, "../../public/img/generated/"),
    { recursive: true, }
  );
  await writeFile(
    resolve(import.meta.dirname, "../../public/img/generated/favicon.png"),
    png,
  );
  await writeFile(
    resolve(import.meta.dirname, "../../public/img/generated/favicon.svg"),
    svg,
  );
}
