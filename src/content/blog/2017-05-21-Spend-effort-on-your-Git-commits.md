---
title: Spend effort on your Git commits
pubDate: 2017-05-21
licence: cc-by
tags:
  - Git
  - VCS
  - Engineering Practices
alias:
  - Commits-Are-Documentation
---

Version control systems like Git are widely appreciated for their ability to provide a centralised location for source code, for helping people work together on the same code base, and for allowing you to scrap the crap you just wrote and get your code back to the state it was in before you started your misguided refactoring.

However, there's one thing it can do for you that is overlooked too often: help you **document your code**. In fact, it can prove to be at least as useful as comments.

Let me show you why.

## What commits can do for you

An important tool in Git is [`git blame`](https://www.git-scm.com/docs/git-blame). Apart from telling you which blockhead introduced an especially ill-conceived piece of code (you, a month ago), it also allows your editor to provide a lot more context to your code. For each line of code, it not only lists the author, but also the commit that last touched it.

Thus, your editor can show the commit messages associated with each line, providing more guidance on why it was introduced. It can tell you why a configuration option was set to a specific value. And in contrast to regular comments, commit messages are directly linked to the version of the code they apply to, so they will never become outdated.

![](./img/Commits-are-documentation/annotations.png)

You can also inspect each commit to find out which _other_ lines were changed, in which files, at the same time. Since most behaviour can only be implemented by modifying your code in multiple places, it often helps to see those changes together to understand why they were needed. And if you see a stray comment that no longer makes sense, its commit will often tell you what was actually meant.

A commit can also serve as documentation on how to achieve a certain task. For example, I recently converted this blog from [Hexo](https://hexo.io/) to [Jekyll](https://jekyllrb.com/) (my apologies if you are subscribed to [my feed](https://vincenttunru.com/atom.xml) and saw duplicate posts). By referring to [a single post's migration commit](https://gitlab.com/VincentTunru/blog/commit/dae75d6efc7d88503bf0d5d3f6a9750d27f2199b), I could see exactly what steps were needed to migrate a post — and which step I forgot when one of them didn't show up properly. Apart from being useful as a reference for yourself, this can also be helpful in instructing other contributors: "to add a new endpoint to the API, take a look at this commit for inspiration".

## Crafting your commits

The above is very useful, but it works best if you invest at least _some_ effort into your commits.

This means that you put some thought into your commit messages and how they document your changes. Make sure they are descriptive enough, that they still make sense a week from now, and that you list any trade-offs you might have had to make — although you might also want to accompany this with a comment to that effect.

[![](https://imgs.xkcd.com/comics/git_commit.png)](https://xkcd.com/1296/)

It also means that you think about what to include in a commit: does it make sense to see these changes together? There's a reason Git has the concept of a [staging area](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics#_the_three_states): it is not necessarily the case that all changes in your working directory make sense in the same commit. As an added bonus, this also makes it more feasible to [revert](https://git-scm.com/docs/git-revert) faulty work.

So basically, try to avoid `git commit -am "asdf"`, and commit consciously. Unless, of course, there's an emergency.

![In case of fire: `git commit`, `git push`, then leave the building.](https://raw.githubusercontent.com/louim/in-case-of-fire/d4f070b18b3d1f416e93762f98507f84708af93a/in_case_of_fire.png)
