---
title: A Fediverse app built on Solid
pubDate: 2025-01-31
licence: cc-by
tags:
  - Solid
  - Fediverse
  - Mastodon
description: I wrote a Solid app to post to the Fediverse. It worked, but…
---

A while back I wrote a [Solid](https://solidproject.org) app that let you use your Pod to post to the Fediverse. This post will mostly be interesting to the select few familiar with both, but some obligatory background info:

# A what now?

Solid is a project that aims to give you more control over your data. As opposed to the popular model where an app silos your data on the makers' servers (e.g. your Facebook posts are on Facebook's computers), Solid apps store your data in a place of your choosing — your _Pod_.

The Fediverse is a set of social media sites that can talk to each other, forming one big social network. For example, I'm a member of fosstodon.org, but I also follow e.g. [Angelo Veltens' videos on tchncs](https://tube.tchncs.de/accounts/aveltens), or [Tim Berners-Lee's posts on w3c.social](https://w3c.social/@timbl). You might have heard of Mastodon; that's the Fediverse. These sites communicate using the [ActivityPub](https://en.wikipedia.org/wiki/ActivityPub) protocol.

# Why

These projects share the same ideal: to help us avoid getting shackled to individual providers that may not have our bests interests in mind, where those shackles are our data or our connections to other people, respectively. Moreover, there's some technological overlap, most specifically in their use of [RDF](https://en.wikipedia.org/wiki/Resource_Description_Framework). And, not unimportantly: the aformentioned Angelo [had already proven](https://datenwissen.de/2020/07/mit-solid-nach-mastodon-posten-geht-das#it) that it could be done!

The projects also have some downsides that this combination might be able to alleviate. On the Fediverse, I might use Mastodon for a Twitter-like experience, and Pixelfed for a photo-focused one, but both expect me to create a completely separate account. Whereas Solid has basically no network effects, so it would be interesting to be able to start using it for social media without requiring all my friends to adopt it as well.

# Stop burying the lede already

Alright, alright, geez, no need to get impatient. You can try it at [martin.vincenttunru.com](https://martin.vincenttunru.com/) (yes, it's called Martin, whatever, naming is hard). However, be aware of some caveats:

- It's still _very_ limited. Most importantly, you can't see other people's posts.
- I've only really tested it with Mastodon, so it ~~might not actually~~ probably won't work with other Fediverse software.
- I've only really tested it with the [NSS](https://github.com/nodeSolidServer/node-solid-server/) implementation of Solid hosted at [solidcommunity.net](https://solidcommunity.net/), so it only really works with that. [For now](https://forum.solidproject.org/t/upcoming-software-switch-for-solidcommunity-net/8608), that is.

# So is this feasible?

Well, it works! But unfortunately, no. Or at least, not with the constraints I set myself, which is that I wanted everything to run inside your browser. The reason for that constraint is that the whole point of Solid is that you know where your data is going — to your Pod. Thus, in an ideal world, you can see that an app is only sending data there; while it can be more efficient for an app to connect to your Pod from their servers, as soon as your data is on someone else's server, you don't know where else it might go.

But: this is asking a lot of your browser. It will have to contact all servers that you are following people on, and process all their posts, and you have to wait for all that to finish before you're able to see your timeline. I do not expect this to scale.

And in fact, the main reason my app works is because it barely uses your Solid Pod at all. Follow requests, likes, notifications, etc. would all get delivered to your Solid Pod, which is neat, but when you post something with Martin, all it has to do is send a notification to the Mastodon servers of your followers, just like a regular Mastodon server would do.

Well… Sort of, at least. Modern browsers don't just let you send requests willy-nilly to any random server unless that server explicitly [says that that is OK to do](https://developer.mozilla.org/docs/Web/HTTP/Headers/Access-Control-Allow-Origin) — which Mastodon servers don't. It would be relatively straightforward for them to allow it, though. Meanwhile, a workaround is to send those requests through my server, which I _just_ said I didn't want to do. That said, my server doesn't get the credentials to your Pod, so by itself it wouldn't be able to interact with either your Pod or with the Fediverse in your name.

If you're intersted, a couple of other small caveats are listed in [Martin's readme](https://gitlab.com/Vinnl/martin/-/blob/main/README.md).

# Now what?

While this was a fun prototype that proved the concept _possible_, it also showed that it wasn't really _feasible_. That said, there's another way to reach the same goals: instead of building a Solid _app_ that connects to the Fediverse, maybe your Solid _Pod_ could do so? Presumably, you trust the server hosting your Pod, so you'd still have the benefit of being in control of your data, and of different apps being able to look at that data, while still being connected to the Fediverse. And that just so happens to be exactly the goal of the [ActivityPods](https://activitypods.org/) project!

I'm not 100% what this means for the future of Solid; I don't think it's feasible that every useful application is shipped as part of your server. (Here's [an alternative proposal](https://forum.solidproject.org/t/why-backend-for-frontend-for-solid-is-categorically-wrong/6673) for those interested.) That said, I'll be following ActivityPods with interest, and meanwhile keep posting on the Fediverse with my [existing account](https://fosstodon.org/@VincentTunru).
