---
title: "Up-and-coming: Reactive programming in Javascript"
pubDate: 2016-06-18 16:21:48
licence: cc-by
tags:
  - Cycle.js
  - Reactive Programming
  - RxJS
  - React
  - Redux
  - Javascript
alias:
  - Life-after-React-and-Redux-Cycle-js
---

Previously, [I took a look at the current state of Javascript frameworks](https://vincenttunru.com/2016-javascript-framework-overview/). Today, I will take a look at what I think will be the next frontier of front-end programming: functional reactive programming.

## (Functional) Reactive Programming

Reactive Programming revolves around the use of a data structure representing asynchronous data, called _observables_ or _streams_. You could compare them to [Promises](http://www.html5rocks.com/en/tutorials/es6/promises/).

![A promise returning the value 3](./img/Javascript-reactive-programming/promise.png)

(Diagrams are courtesy of [RxMarbles.com](http://rxmarbles.com). The arrows represent time.)

Roughly speaking, promises represent a value that might not be known when they are first used, but might arrive any moment. Observables are similar, except that they represent the potential arrival of multiple values.

![An observable returning the values 1, 2 and 3, in that order](./img/Javascript-reactive-programming/stream.png)

Observables are great for representing related values arriving asynchronously, such as characters entered into an input field.

When we use functions that take observables as input and produce a new observable as output (i.e. [pure functions](https://en.wikipedia.org/wiki/Pure_function)), we are doing what is commonly referred to as _Functional Reactive Programming_. There are many Javascript libraries providing commonly used operators, such as [RxJS](https://github.com/Reactive-Extensions/RxJS), [Bacon.js](https://baconjs.github.io/) and [Kefir](https://rpominov.github.io/kefir/).

These can be simple operators like [`delay`](http://rxmarbles.com/#delay), that simply returns a new stream that produces the same values as the input stream, only with a slight delay.

![Example of `delay`](./img/Javascript-reactive-programming/delay.png)

The operators might use the actual values produced like [`map`](http://rxmarbles.com/#map), which returns a new stream that produces the values of the input stream after being passed through a given function.

![Example of `map`](./img/Javascript-reactive-programming/map.png)

Previous values could also be used, like in [`scan`](http://rxmarbles.com/#scan), which produces an accumulator using a given function (similar to the [`reduce`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce) function you might know already).

![Example of `scan`](./img/Javascript-reactive-programming/scan.png)

They might even combine multiple streams like [`merge`](http://rxmarbles.com/#merge), which returns a stream that produces all the values produced by its input streams.

![Example of `merge`](./img/Javascript-reactive-programming/merge.png)

One you get used to them, observables can greatly simplify dealing with asynchronous data. For example, consider a situation where you have two input fields, and you want to check whether both of them have a value. When you have observables `field1$` and `field2$` (convention is to denote observables by appending a `$`), you can use RxJS to create a stream that emits a new boolean every time the form changes validity:

```js
const isValid$ = Rx.Observable.combineLatest(field1$, field2$).map((fields) =>
  fields.every((field) => field.length > 0),
);
```

(You can see it in action in [this fiddle](https://jsfiddle.net/vhb4jcw9/).)

This might be difficult to parse the first time you read it, but once it clicks, working with asynchronous data becomes far easier with observables in our toolbox. Note that the above approach easily scales to however many form fields you have.

An additional benefit is that it steers you away from a whole class of bugs. To illustrate: consider trying to do the above using a more traditional approach. We would likely [create an event listener to check the validity of the form](https://jsfiddle.net/tn32yvmm/) when the user enters input:

```js
function checkValidity() {
  return (
    document.getElementById("field1").value.length > 0 &&
    document.getElementById("field2").value.length > 0
  );
}
```

We wouldn't be the first to attach the event listener to the final form field and be done with it. We fill in the form, see the validity update, and conclude that it works. After deployment, however, some users will fill out the form, and then go back to the first field and clear its contents. Surprise: our form will still claim that it's valid. We forgot that the event handler also had to be attached to the other form field.

Obviously, what with you being the perfect programmer, this wouldn't happen to you. But by using observable, such errors would be really easy to spot. When your less gifted colleague adds is tasked with adding a form field, there's only one place it needs to be added: the combined form stream. It will then just work, validation included, as the data flows directly from the form fields into the validation.

There is much more to Functional Reactive Programming than this, and fully wrapping your head around it might take some time. If you're interested, you might want to try [Getting Started with RxJS](https://github.com/Reactive-Extensions/RxJS/tree/master/doc#getting-started-with-rxjs).

## FRP today

If you're as excited about FRP as I am, you're in luck: it's already gaining a lot of headway.

Consider, for example, the combination of React and Redux I touched upon in [my previous post](https://vincenttunru.com/2016-javascript-framework-overview/). With Redux, you describe how your application's state evolves in response to user actions with a function of the form `(currentState, action) => newState`. You then use React to describe what view that state should result in with a function of the form `state => view`.

Wait a minute... If you squint hard enough, that looks just like manipulating an observable with the [`scan()`](https://github.com/Reactive-Extensions/RxJS/blob/master/doc/api/core/operators/scan.md) and [`map()`](https://github.com/Reactive-Extensions/RxJS/blob/master/doc/api/core/operators/select.md) methods!

![React/Redux's architecture modelled](./img/Javascript-reactive-programming/react_architecture.png)

This is a very limited reactive system. In essence, all asynchronous input to your application will have to be converted to that one observable for actions. This can get quite awkward for asynchronous input that isn't exactly a user action, such as HTTP requests. This often leads to workarounds such as [emitting multiple actions](http://redux.js.org/docs/advanced/AsyncActions.html), e.g. one when a request is sent, and another when the response comes in. [Several libraries have arisen](https://twitter.com/dan_abramov/status/689639582120415232) that claim to make it easier, but all of them are bound to that single stream.

A Redux alternative called [MobX](https://mobxjs.github.io/mobx/) is also gaining ground, providing an implementation of observables to model component state. I haven't looked into it too deeply yet, but it appears to distinguish itself from Redux mainly by enabling components' local state to be modelled as observables as well, whereas Redux only supports a single place to hold all your application state.

On the Angular side, the developers have decided to [endorse and use RxJS](https://angular.io/docs/ts/latest/guide/server-communication.html#!#rxjs) for the major rewrite that they are currently completing. They're using it for HTTP requests, the planned router [is using it as well](http://blog.thoughtram.io/angular/2016/06/14/routing-in-angular-2-revisited.html), and you can pass them to your view templates [for more efficient rendering](http://victorsavkin.com/post/110170125256/change-detection-in-angular-2). That said, user input is still handled through callbacks, and in practise, many Angular apps will likely apply none or few transformations on their observables before passing its values back to regular callback- and promise-based code.

## A glimpse of the future: Cycle.js

The first steps toward wider adoption of FRP have been taken, and they don't seem to be stopping any day soon. So where will this bring us?

One framework that is worth looking into in this regard is [Cycle.js](http://cycle.js.org/). Cycle applications are conceptually simple: all they do is convert a series of input streams (_sources_, in Cycle terminology) to a series of output streams (_sinks_). These input streams are provided by what Cycle dubs _drivers_, and the output streams are fed back into those drivers. The values emitted to the output streams are descriptions of the [side effects](<https://en.wikipedia.org/wiki/Side_effect_(computer_science)>) you want to happen, and the drivers actually execute them, looping potential results back into the input streams. These drivers are usually small, separate libraries tailor-made to perform a specific side-effect in response to incoming observables, and to deliver any potential outside values through observables as well. A consequence is that the app itself is side-effect free, making it easy to reason about and easy to test.

Let's illustrate this with a very simple example application.

![](./img/Javascript-reactive-programming/cycle_lifecycle_app.png)

To be able to show something to the user, we output a description of what we want the DOM to look like onto the DOM driver's output stream. The DOM driver will then apply those changes to the DOM.

![](./img/Javascript-reactive-programming/cycle_lifecycle_render.png)

Likewise, the DOM driver also provides input streams that contains user input. Which can, in turn, be a trigger to push new DOM descriptions onto the output stream and thus update the view.

![](./img/Javascript-reactive-programming/cycle_lifecycle_input.png)

User interaction is only one type of side effect though. Another commonly used driver is the HTTP driver. By pushing values onto the output stream to the HTTP driver, apps can send HTTP requests, and the accompanying responses will be pushed onto the corresponding input stream.

![](./img/Javascript-reactive-programming/cycle_lifecycle.png)

The DOM and HTTP drivers are just the tip of the iceberg. There are plenty more drivers imaginable, such as the [history driver](https://github.com/cyclejs/history) for the browser's [History API](https://developer.mozilla.org/docs/Web/API/History_API), a [storage driver](https://github.com/cyclejs/storage) for interacting with [localStorage](https://developer.mozilla.org/docs/Web/API/Window/localStorage), [and more](https://github.com/cyclejs-community/awesome-cyclejs#drivers). Of course, you can also [write your own](http://cycle.js.org/drivers.html).

## Wrapping up

As we saw, the combination of observables and functional programming principles can greatly simplify working with asynchronicity such as often abundant in web applications, and that is still difficult to deal with in [the current crop of Javascript frameworks](https://vincenttunru.com/2016-javascript-framework-overview/). It's been getting more and more uptake as React developers slowly get familiar with a reactive programming style, and Angular developers set to have the full power of RxJS available in version 2. Cycle.js shows, however, that observables can be used far more extensively, for all that is asynchronous, in an elegant application architecture.
