---
draft: true
title: Keep your code GREAS'D, not DRY
licence: cc-by
tags:
  - Engineering
pubDate: 1337-01-01
---

- Premature abstraction makes your code _less_ flexible
- Not always intentional: e.g. CSS (style for `h1` - always an exception), Web Components (communicate through events - who listens, who throws?)
- Implement abstractions, but only such that they apply to _exactly_ your use cases, and no more

Funny acronyms: GREAS(E)D, OILED
Terms to fit in there:

- opt-in abstraction
- exactly as needed
- limited scope

- Feasible testing (esp. vs. integrated in back-end a la Django)
