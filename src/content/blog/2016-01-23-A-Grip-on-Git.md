---
title: "A Grip on Git: An interactive, visual Git tutorial"
pubDate: 2016-01-23 16:18:40
licence: cc-by
tags:
  - Projects
alias:
  - aGripOnGit
---

Git is one of the most important tools in software development right now. It doesn't matter what sector your company is active in, what programming language you use or whether you do waterfall, scrum, kanban or what not; the most common denominator is going to be version control. And most of the time, you'll be using Git to do it.

Unfortunately, knowledge of Git is often limited to [memorising a few commands](https://xkcd.com/1597/) like `git add` and `git commit`, sometimes copy-pasting a `git clone` or a `git pull`, and every now and then finding a StackOverflow answer that spells out the exact command needed to merge, delete a branch, or perhaps even to rebase. And sure, this will get you by most of the time.

Every now and then, however, it will blow up in your face: you might have gotten stuck in a headless state, perhaps you thought a force push was a good idea, or you might mess up some other way. Not pleasant. But what's even worse: **you're likely giving up on Git's best features!** Proper understanding of Git, and the resulting proper workflow, can prevent many programming errors and thus improve the quality of your code. And when problems do slip through the cracks, a clean commit history is excellent documentation for your code, allowing you to more easily spot potential fixes.

# Introducing: A grip on Git

Thus, I wrote [A grip on Git](//agripongit.vincenttunru.com/), a tutorial aimed at those who use Git at a basic level but don't really know what the commands they copy-paste actually do. It's a short read (about eleven minutes) that quickly describes a typical Git workflow, accompanied by a visualisation that displays what happens behind the scenes as you read/scroll. My hope is that this will help people get a better grasp of Git: being able to visualise what is happening should help in remembering what commands you need when working with Git.

Of course, my main goal working on this project was to learn a few new things -- about which I should be able to blog shortly. Nevertheless, I do hope it will be useful to some, as I sincerely believe Git is a very important tool. So [go check it out!](//agripongit.vincenttunru.com/)
