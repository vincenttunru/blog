---
title: toBeTruthy() will bite you — use toBe(true)
pubDate: 2016-09-22 07:07:45
licence: cc-by
tags:
  - Jasmine
  - Protractor
  - Angular
---

Here's a short public service announcement for those who write their tests using [Jasmine](https://jasmine.github.io/) or [Protractor](http://www.protractortest.org/): avoid [the `toBeTruthy()` and `toBeFalsy()` matchers](https://jasmine.github.io/2.5/introduction.html#section-Included_Matchers) and use `toBe(true)` and `toBe(false)` instead.

(Off topic: I've also seen people confuse Jasmine and [Karma](https://www.npmjs.com/package/karma). Karma fires up a browser to run tests in, but the actual tests are written using a testing library such as Jasmine.)

![](./img/toBeTruthy-vs-toBe-true/no_toBeTruthy_allowed.svg)

# What's the problem?

I've seen people using these matchers assuming they're just a weird spelling of `toBeTrue()` and `toBeFalse()`. But they're not: truthy and falsy [refer](https://developer.mozilla.org/en-US/docs/Glossary/Truthy) to values that are evaluated to `true` and `false` _after being [coerced](https://developer.mozilla.org/en-US/docs/Glossary/Type_Conversion) to a boolean_!

So yes, `true` is truthy, but `42` is _also_ truthy, and even `[]` and `'false'` are truthy! In fact, everything that is not `0`, `""`, `null`, `undefined`, `NaN` or `false` is truthy.

This means that if you're testing a function that errorenously returns `'false'` (a string) instead of `false` (a boolean), `toBeTruthy()` will match and `toBeFalsy()` will not.

So just like [you should never use `==`](http://eslint.org/docs/rules/eqeqeq), try to avoid `toBeTruthy()` and `toBeFalsy()`.

# So then what should I use?

Just use [the plain old `toBe()`](https://jasmine.github.io/2.5/introduction.html#section-Matchers) matcher to check for a value `toBe(true)` or `toBe(false)`, respectively. Unless you actually want your function to return different types of values, but even then I'd recommend you to change your code: otherwise, everywhere you're calling that function, you have to check for all possible return types.
