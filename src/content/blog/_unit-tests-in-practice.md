---
draft: true
pubDate: 1337-01-01
licence: cc-by
title: Unit tests in practice
tags:
  - Engineering Practices
---

This article is not about which unit test framework to use, or what is syntax looks like — there's plenty of that to go around. What's less obvious is how to put that in practice: how do you start writing tests for your actual code, in practice? I'll share what has worked for me, in the hope that it works for you too.

## Step 0: your code

In my experience, the primary way to make it feasible to add tests to your code, is to adjust how you write your code. Specifically, by making most of your code reside in [pure functions](https://en.wikipedia.org/wiki/Pure_function): functions that do not change anything outside of its own scope, and that return the same value for the same input parameters, every time.

You probably won't be able to write _all_ your code like this. But you can do it for _most_ of it, and the remaining code is straightforward enough that it is unlikely to contain bugs. And as an added benefit, you might find that it makes your code easier to read too!
