---
draft: true
title: Current challenges as a Solid application developer
pubDate: 1337-01-01
licence: cc-by
tags:
  - Solid
---

# No space:storage (/optionality/no "Solid" spec, just a recommended collection of other specs, so you can't assume anything)

https://gitter.im/solid/specification?at=61698dc7fb3dcd4e882aae3c

As [TimBL says](https://gitter.im/solid/specification?at=5fccb3062d8b7c763845b886), "Every “may” is a weakness in the system".
