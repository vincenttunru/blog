---
title: Towards better bug reports
pubDate: 2018-08-03
licence: cc-by
tags:
  - Open source
  - Engineering Practices
alias:
  - better-bug-reports
---

I just implemented a little change that I think will greatly improve the quality of bug reports I receive: I created a minimal boilerplate repo for my project.

The best bug reports are [minimal, complete, and verifable](https://stackoverflow.com/help/mcve). In other words:

- The maintainer should not be distracted by things not relevant to the bug.
- All info relevant to the bug should be included.
- The maintainer should be able to observe the buggy behaviour by themselves.

Submitting a high-quality bug report is not always easy though, which can often lead to it not being worth the effort to report. Consider, for example, a package I maintain: [`wdio-webpack-dev-server-service`](https://www.npmjs.com/package/wdio-webpack-dev-server-service). To create a minimal, complete and verifiable example for a bug report, you would have to:

- create a barebones Webpack project
- set up WebdriverIO and add a minimal test
- set up wdio-webpack-dev-server-service
- and of course: reproduce your bug

That's _three_ tools to configure! And since it's probably been a while since you've set up those tools for your own project, that's three tools for which you'll have to dig up the documentation again.

To save my users the effort and thus hopefully encourage high-quality bug reports, I created [a repository with a barebones project](https://gitlab.com/Vinnl/wdio-webpack-dev-server-service-minimal-repro). The repository contains all the boilerplate and instructions needed to get `wdio-webpack-dev-server-service` up and running. I then updated the issue template to refer to that repository and to encourage bug reporters to use it to create a minimal, complete, and verifiable example.

Now bring on those bug reports.
