// @ts-check
import { defineConfig, envField } from "astro/config";
import mdx from "@astrojs/mdx";
import sitemap from "@astrojs/sitemap";

import tailwindcss from "@tailwindcss/vite";

import react from "@astrojs/react";
import { createFavicon } from "./src/functions/createFavicon";
import { createFallbackOpenGraphImage } from "./src/functions/createOpenGraphImage";

createFavicon();
createFallbackOpenGraphImage();

// https://astro.build/config
export default defineConfig({
  site:
    process.env.CI_COMMIT_BRANCH === "draft"
      ? typeof process.env.CI_PAGES_HOSTNAME === "string"
        ? `https://${process.env.CI_PAGES_HOSTNAME}`
        : "https://VincentTunru.gitlab.io"
      : process.env.CI
        ? "https://VincentTunru.com"
        : "http://localhost:4321",
  base: process.env.CI_COMMIT_BRANCH === "draft" ? "blog" : undefined,
  integrations: [mdx(), sitemap(), react()],
  env: {
    schema: {
      CI_COMMIT_BRANCH: envField.string({
        context: "client",
        access: "public",
        optional: true,
      }),
    },
  },
  prefetch: {
    defaultStrategy: "hover",
    prefetchAll: true,
  },
  vite: {
    plugins: [tailwindcss()],
  },
  experimental: {
    responsiveImages: true,
  },
  redirects: {
    "/about/": "/",
  },
});
